<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:kitt3n_contact/Resources/Private/Language/locallang_db.xlf:tx_kitt3ncontact_domain_model_person',
        'label' => 'gender',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'sortby' => 'sorting',
        'versioningWS' => true,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'title,firstname,middlename,lastname,description,address,city,zip,region,country,email,phone,mobile,fax,website,birthday,position,company,building,room,skype,twitter,facebook,instagram,snapchat,linkedin,xing,latitude,longitude',
        'iconfile' => 'EXT:kitt3n_contact/Resources/Public/Icons/tx_kitt3ncontact_domain_model_person.gif'
    ],
    'interface' => [
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, gender, title, firstname, middlename, lastname, image, description, address, city, zip, region, country, email, phone, mobile, fax, website, birthday, position, company, building, room, skype, twitter, facebook, instagram, snapchat, linkedin, xing, latitude, longitude',
    ],
    'types' => [
        '1' => ['showitem' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, gender, title, firstname, middlename, lastname, image, description, address, city, zip, region, country, email, phone, mobile, fax, website, birthday, position, company, building, room, skype, twitter, facebook, instagram, snapchat, linkedin, xing, latitude, longitude, --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access, starttime, endtime'],
    ],
    'columns' => [
        'sys_language_uid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'special' => 'languages',
                'items' => [
                    [
                        'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.allLanguages',
                        -1,
                        'flags-multiple'
                    ]
                ],
                'default' => 0,
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'default' => 0,
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_kitt3ncontact_domain_model_person',
                'foreign_table_where' => 'AND {#tx_kitt3ncontact_domain_model_person}.{#pid}=###CURRENT_PID### AND {#tx_kitt3ncontact_domain_model_person}.{#sys_language_uid} IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        't3ver_label' => [
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.versionLabel',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ],
        ],
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.visible',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'items' => [
                    [
                        0 => '',
                        1 => '',
                        'invertStateDisplay' => true
                    ]
                ],
            ],
        ],
        'starttime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int',
                'default' => 0,
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ]
            ],
        ],
        'endtime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int',
                'default' => 0,
                'range' => [
                    'upper' => mktime(0, 0, 0, 1, 1, 2038)
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ]
            ],
        ],

        'gender' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_contact/Resources/Private/Language/locallang_db.xlf:tx_kitt3ncontact_domain_model_person.gender',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int'
            ]
        ],
        'title' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_contact/Resources/Private/Language/locallang_db.xlf:tx_kitt3ncontact_domain_model_person.title',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'firstname' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_contact/Resources/Private/Language/locallang_db.xlf:tx_kitt3ncontact_domain_model_person.firstname',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'middlename' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_contact/Resources/Private/Language/locallang_db.xlf:tx_kitt3ncontact_domain_model_person.middlename',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'lastname' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_contact/Resources/Private/Language/locallang_db.xlf:tx_kitt3ncontact_domain_model_person.lastname',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'image' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_contact/Resources/Private/Language/locallang_db.xlf:tx_kitt3ncontact_domain_model_person.image',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                'image',
                [
                    'appearance' => [
                        'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:images.addFileReference'
                    ],
                    'foreign_types' => [
                        '0' => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ]
                    ],
                    'maxitems' => 1
                ],
                $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
            ),
        ],
        'description' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_contact/Resources/Private/Language/locallang_db.xlf:tx_kitt3ncontact_domain_model_person.description',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim'
            ]
        ],
        'address' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_contact/Resources/Private/Language/locallang_db.xlf:tx_kitt3ncontact_domain_model_person.address',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim'
            ]
        ],
        'city' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_contact/Resources/Private/Language/locallang_db.xlf:tx_kitt3ncontact_domain_model_person.city',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'zip' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_contact/Resources/Private/Language/locallang_db.xlf:tx_kitt3ncontact_domain_model_person.zip',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'region' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_contact/Resources/Private/Language/locallang_db.xlf:tx_kitt3ncontact_domain_model_person.region',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'country' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_contact/Resources/Private/Language/locallang_db.xlf:tx_kitt3ncontact_domain_model_person.country',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'email' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_contact/Resources/Private/Language/locallang_db.xlf:tx_kitt3ncontact_domain_model_person.email',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'phone' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_contact/Resources/Private/Language/locallang_db.xlf:tx_kitt3ncontact_domain_model_person.phone',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'mobile' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_contact/Resources/Private/Language/locallang_db.xlf:tx_kitt3ncontact_domain_model_person.mobile',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'fax' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_contact/Resources/Private/Language/locallang_db.xlf:tx_kitt3ncontact_domain_model_person.fax',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'website' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_contact/Resources/Private/Language/locallang_db.xlf:tx_kitt3ncontact_domain_model_person.website',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'birthday' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_contact/Resources/Private/Language/locallang_db.xlf:tx_kitt3ncontact_domain_model_person.birthday',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'position' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_contact/Resources/Private/Language/locallang_db.xlf:tx_kitt3ncontact_domain_model_person.position',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'company' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_contact/Resources/Private/Language/locallang_db.xlf:tx_kitt3ncontact_domain_model_person.company',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'building' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_contact/Resources/Private/Language/locallang_db.xlf:tx_kitt3ncontact_domain_model_person.building',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'room' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_contact/Resources/Private/Language/locallang_db.xlf:tx_kitt3ncontact_domain_model_person.room',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'skype' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_contact/Resources/Private/Language/locallang_db.xlf:tx_kitt3ncontact_domain_model_person.skype',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'twitter' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_contact/Resources/Private/Language/locallang_db.xlf:tx_kitt3ncontact_domain_model_person.twitter',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'facebook' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_contact/Resources/Private/Language/locallang_db.xlf:tx_kitt3ncontact_domain_model_person.facebook',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'instagram' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_contact/Resources/Private/Language/locallang_db.xlf:tx_kitt3ncontact_domain_model_person.instagram',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'snapchat' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_contact/Resources/Private/Language/locallang_db.xlf:tx_kitt3ncontact_domain_model_person.snapchat',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'linkedin' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_contact/Resources/Private/Language/locallang_db.xlf:tx_kitt3ncontact_domain_model_person.linkedin',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'xing' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_contact/Resources/Private/Language/locallang_db.xlf:tx_kitt3ncontact_domain_model_person.xing',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'latitude' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_contact/Resources/Private/Language/locallang_db.xlf:tx_kitt3ncontact_domain_model_person.latitude',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'longitude' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_contact/Resources/Private/Language/locallang_db.xlf:tx_kitt3ncontact_domain_model_person.longitude',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
    
    ],
];
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder