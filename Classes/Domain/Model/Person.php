<?php
namespace KITT3N\Kitt3nContact\Domain\Model;


/***
 *
 * This file is part of the "kitt3n | Contact" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 
 *
 ***/
/**
 * Person
 */
class Person extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * gender
     * 
     * @var int
     */
    protected $gender = 0;

    /**
     * title
     * 
     * @var string
     */
    protected $title = '';

    /**
     * firstname
     * 
     * @var string
     */
    protected $firstname = '';

    /**
     * middlename
     * 
     * @var string
     */
    protected $middlename = '';

    /**
     * lastname
     * 
     * @var string
     */
    protected $lastname = '';

    /**
     * image
     * 
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @cascade remove
     */
    protected $image = null;

    /**
     * description
     * 
     * @var string
     */
    protected $description = '';

    /**
     * address
     * 
     * @var string
     */
    protected $address = '';

    /**
     * city
     * 
     * @var string
     */
    protected $city = '';

    /**
     * zip
     * 
     * @var string
     */
    protected $zip = '';

    /**
     * region
     * 
     * @var string
     */
    protected $region = '';

    /**
     * country
     * 
     * @var string
     */
    protected $country = '';

    /**
     * email
     * 
     * @var string
     */
    protected $email = '';

    /**
     * phone
     * 
     * @var string
     */
    protected $phone = '';

    /**
     * mobile
     * 
     * @var string
     */
    protected $mobile = '';

    /**
     * fax
     * 
     * @var string
     */
    protected $fax = '';

    /**
     * website
     * 
     * @var string
     */
    protected $website = '';

    /**
     * birthday
     * 
     * @var string
     */
    protected $birthday = '';

    /**
     * position
     * 
     * @var string
     */
    protected $position = '';

    /**
     * company
     * 
     * @var string
     */
    protected $company = '';

    /**
     * building
     * 
     * @var string
     */
    protected $building = '';

    /**
     * room
     * 
     * @var string
     */
    protected $room = '';

    /**
     * skype
     * 
     * @var string
     */
    protected $skype = '';

    /**
     * twitter
     * 
     * @var string
     */
    protected $twitter = '';

    /**
     * facebook
     * 
     * @var string
     */
    protected $facebook = '';

    /**
     * instagram
     * 
     * @var string
     */
    protected $instagram = '';

    /**
     * snapchat
     * 
     * @var string
     */
    protected $snapchat = '';

    /**
     * linkedin
     * 
     * @var string
     */
    protected $linkedin = '';

    /**
     * xing
     * 
     * @var string
     */
    protected $xing = '';

    /**
     * latitude
     * 
     * @var string
     */
    protected $latitude = '';

    /**
     * longitude
     * 
     * @var string
     */
    protected $longitude = '';

    /**
     * Returns the gender
     * 
     * @return int $gender
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Sets the gender
     * 
     * @param int $gender
     * @return void
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    /**
     * Returns the title
     * 
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     * 
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the firstname
     * 
     * @return string $firstname
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Sets the firstname
     * 
     * @param string $firstname
     * @return void
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    /**
     * Returns the middlename
     * 
     * @return string $middlename
     */
    public function getMiddlename()
    {
        return $this->middlename;
    }

    /**
     * Sets the middlename
     * 
     * @param string $middlename
     * @return void
     */
    public function setMiddlename($middlename)
    {
        $this->middlename = $middlename;
    }

    /**
     * Returns the lastname
     * 
     * @return string $lastname
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Sets the lastname
     * 
     * @param string $lastname
     * @return void
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    /**
     * Returns the image
     * 
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Sets the image
     * 
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
     * @return void
     */
    public function setImage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $image)
    {
        $this->image = $image;
    }

    /**
     * Returns the description
     * 
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Sets the description
     * 
     * @param string $description
     * @return void
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Returns the address
     * 
     * @return string $address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Sets the address
     * 
     * @param string $address
     * @return void
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * Returns the city
     * 
     * @return string $city
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Sets the city
     * 
     * @param string $city
     * @return void
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * Returns the zip
     * 
     * @return string $zip
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * Sets the zip
     * 
     * @param string $zip
     * @return void
     */
    public function setZip($zip)
    {
        $this->zip = $zip;
    }

    /**
     * Returns the region
     * 
     * @return string $region
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Sets the region
     * 
     * @param string $region
     * @return void
     */
    public function setRegion($region)
    {
        $this->region = $region;
    }

    /**
     * Returns the country
     * 
     * @return string $country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Sets the country
     * 
     * @param string $country
     * @return void
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * Returns the email
     * 
     * @return string $email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Sets the email
     * 
     * @param string $email
     * @return void
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * Returns the phone
     * 
     * @return string $phone
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Sets the phone
     * 
     * @param string $phone
     * @return void
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * Returns the mobile
     * 
     * @return string $mobile
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * Sets the mobile
     * 
     * @param string $mobile
     * @return void
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;
    }

    /**
     * Returns the fax
     * 
     * @return string $fax
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Sets the fax
     * 
     * @param string $fax
     * @return void
     */
    public function setFax($fax)
    {
        $this->fax = $fax;
    }

    /**
     * Returns the website
     * 
     * @return string $website
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Sets the website
     * 
     * @param string $website
     * @return void
     */
    public function setWebsite($website)
    {
        $this->website = $website;
    }

    /**
     * Returns the birthday
     * 
     * @return string $birthday
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Sets the birthday
     * 
     * @param string $birthday
     * @return void
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;
    }

    /**
     * Returns the position
     * 
     * @return string $position
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Sets the position
     * 
     * @param string $position
     * @return void
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * Returns the company
     * 
     * @return string $company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Sets the company
     * 
     * @param string $company
     * @return void
     */
    public function setCompany($company)
    {
        $this->company = $company;
    }

    /**
     * Returns the building
     * 
     * @return string $building
     */
    public function getBuilding()
    {
        return $this->building;
    }

    /**
     * Sets the building
     * 
     * @param string $building
     * @return void
     */
    public function setBuilding($building)
    {
        $this->building = $building;
    }

    /**
     * Returns the room
     * 
     * @return string $room
     */
    public function getRoom()
    {
        return $this->room;
    }

    /**
     * Sets the room
     * 
     * @param string $room
     * @return void
     */
    public function setRoom($room)
    {
        $this->room = $room;
    }

    /**
     * Returns the skype
     * 
     * @return string $skype
     */
    public function getSkype()
    {
        return $this->skype;
    }

    /**
     * Sets the skype
     * 
     * @param string $skype
     * @return void
     */
    public function setSkype($skype)
    {
        $this->skype = $skype;
    }

    /**
     * Returns the twitter
     * 
     * @return string $twitter
     */
    public function getTwitter()
    {
        return $this->twitter;
    }

    /**
     * Sets the twitter
     * 
     * @param string $twitter
     * @return void
     */
    public function setTwitter($twitter)
    {
        $this->twitter = $twitter;
    }

    /**
     * Returns the facebook
     * 
     * @return string $facebook
     */
    public function getFacebook()
    {
        return $this->facebook;
    }

    /**
     * Sets the facebook
     * 
     * @param string $facebook
     * @return void
     */
    public function setFacebook($facebook)
    {
        $this->facebook = $facebook;
    }

    /**
     * Returns the instagram
     * 
     * @return string $instagram
     */
    public function getInstagram()
    {
        return $this->instagram;
    }

    /**
     * Sets the instagram
     * 
     * @param string $instagram
     * @return void
     */
    public function setInstagram($instagram)
    {
        $this->instagram = $instagram;
    }

    /**
     * Returns the snapchat
     * 
     * @return string $snapchat
     */
    public function getSnapchat()
    {
        return $this->snapchat;
    }

    /**
     * Sets the snapchat
     * 
     * @param string $snapchat
     * @return void
     */
    public function setSnapchat($snapchat)
    {
        $this->snapchat = $snapchat;
    }

    /**
     * Returns the linkedin
     * 
     * @return string $linkedin
     */
    public function getLinkedin()
    {
        return $this->linkedin;
    }

    /**
     * Sets the linkedin
     * 
     * @param string $linkedin
     * @return void
     */
    public function setLinkedin($linkedin)
    {
        $this->linkedin = $linkedin;
    }

    /**
     * Returns the xing
     * 
     * @return string $xing
     */
    public function getXing()
    {
        return $this->xing;
    }

    /**
     * Sets the xing
     * 
     * @param string $xing
     * @return void
     */
    public function setXing($xing)
    {
        $this->xing = $xing;
    }

    /**
     * Returns the latitude
     * 
     * @return string $latitude
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Sets the latitude
     * 
     * @param string $latitude
     * @return void
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    }

    /**
     * Returns the longitude
     * 
     * @return string $longitude
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Sets the longitude
     * 
     * @param string $longitude
     * @return void
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    }
}
