<?php
namespace KITT3N\Kitt3nContact\Domain\Repository;


/***
 *
 * This file is part of the "kitt3n | Contact" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 
 *
 ***/
/**
 * The repository for Persons
 */
class PersonRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

    /**
     * @var array
     */
    protected $defaultOrderings = [
        'sorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
    ];

    /**
     * findByUidListOrderByListIfNotEmpty
     *
     * @param string $uidList
     * @param string $orderElements
     * @return \HIVE\HiveExtHistory\Domain\Model\History
     */
    public function findByUidListOrderByListIfNotEmpty($uidList, $orderElements = "ASC")
    {
        $result = [];

        if ($uidList != "") {
            $uidArray = explode(',', $uidList);


            if ($orderElements == "DESC") {
                for ($i = (count($uidArray) - 1); $i >= 0; $i-- ) {
                    $result[] = $this->findByUid($uidArray[$i]);
                }
            } else {
                foreach ($uidArray as $uid) {
                    $result[] = $this->findByUid($uid);
                }
            }
        }

        return $result;

    }
}
