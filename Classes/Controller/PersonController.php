<?php
namespace KITT3N\Kitt3nContact\Controller;


/***
 *
 * This file is part of the "kitt3n | Contact" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 
 *
 ***/
/**
 * PersonController
 */
class PersonController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * personRepository
     * 
     * @var \KITT3N\Kitt3nContact\Domain\Repository\PersonRepository
     * @inject
     */
    protected $personRepository = null;

    /**
     * action list
     * 
     * @return void
     */
    public function listAction()
    {
        // $oPersons = $this->personRepository->findAll();
        $aSettings = $this->settings;
        $oContentObj = $this->configurationManager->getContentObject();
        $iRecordUid = $oContentObj->data['uid'];


        // get selected contacts from flexform
        $sFlexformPersonsUids = $aSettings['sPersons'];
        $oFlexformPersons = $this->personRepository->findByUidListOrderByListIfNotEmpty($sFlexformPersonsUids, "ASC");
        $iFlexformPersonsCount = count($oFlexformPersons);

        // assign objects to fluid template
        $this->view->assign('oPersons', $oFlexformPersons);
        $this->view->assign('aSettings', $aSettings);
        $this->view->assign('iPersonsCount', $iFlexformPersonsCount);
        $this->view->assign('iRecordUid', $iRecordUid);
    }

    /**
     * action
     * 
     * @return void
     */
    public function Action()
    {
    }

    /**
     * action show
     * 
     * @param \KITT3N\Kitt3nContact\Domain\Model\Person $person
     * @return void
     */
    public function showAction(\KITT3N\Kitt3nContact\Domain\Model\Person $person)
    {
        $this->view->assign('person', $person);
    }
}
