var kitt3n_contacts__interval = setInterval(function () {
    if ( typeof kitt3n_windowLoad == 'undefined') {
    } else {

        if (typeof kitt3n_windowLoad == "boolean" && kitt3n_windowLoad) {

            if (kitt3n_applicationContext == "Development") {
                console.info('kitt3n_contact script is loaded!');
            }

            clearInterval(kitt3n_contacts__interval);

            var aContacts = [];
            var oContacts = document.getElementsByClassName('kitt3n_contact-item');

            if (typeof oContacts == "object" && oContacts.length > 0) {
                for (var i = 0; i < oContacts.length; i++) {
                    (function (i) {

                        aContacts[i] = oContacts[i];
                        var oCopyTrigger = aContacts[i].querySelector('.copy-trigger');
                        var oCopyTarget = aContacts[i].querySelector('.copy-target');

                        oCopyTrigger.addEventListener("click", function( event ) {
                            event.preventDefault();

                            var sEncrypted = oCopyTarget.getAttribute('href');
                            copyToClipboard(sEncrypted);

                        });

                    })(i);
                }
            }
        }
    }

}, 1000);


var copyToClipboard = function(s) {

    // console.log(s);

    // get encrypted mail from data attribute
    var patternCaseOne = "%27",
        patternCaseTwo = "'",
        sEncryptedMailTemp,
        sEncryptedMail;

    // replace specific encryption patterns from string
    if(s.indexOf(patternCaseOne) !== -1){
        sEncryptedMailTemp = s.replace("javascript:linkTo_UnCryptMailto(%27", "");
        sEncryptedMail = sEncryptedMailTemp.replace("%27);", "");
    }
    if(s.indexOf(patternCaseTwo) !== -1){
        sEncryptedMailTemp = s.replace("javascript:linkTo_UnCryptMailto('", "");
        sEncryptedMail = sEncryptedMailTemp.replace("');", "");
    }
    // console.log(sEncryptedMail);

    // decrypt mail
    var sDecryptedMailTemp = decryptString(sEncryptedMail, 7);
    var sDecryptedMail = sDecryptedMailTemp.replace("mailto:", "");
    // console.log(sDecryptedMail);

    // copy decrypted mail to clipboard
    var input = document.createElement('textarea');

    input.style.position = 'fixed';
    input.style.top = 0;
    input.style.left = 0;
    input.style.width = '1px';
    input.style.height = '1px';
    input.style.padding = 0;
    input.style.border = 'none';
    input.style.outline = 'none';
    input.style.boxShadow = 'none';
    input.style.background = 'transparent';

    // copy process
    document.body.appendChild(input);
    input.value = sDecryptedMail;
    input.focus();
    input.select();
    document.execCommand('Copy');
    input.remove();

};