<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'KITT3N.Kitt3nContact',
            'Kitt3ncontactpersonlist',
            'Kitt3n Contact List'
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('kitt3n_contact', 'Configuration/TypoScript', 'kitt3n | Contact');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_kitt3ncontact_domain_model_person', 'EXT:kitt3n_contact/Resources/Private/Language/locallang_csh_tx_kitt3ncontact_domain_model_person.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_kitt3ncontact_domain_model_person');

    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder